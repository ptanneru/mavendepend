<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">

	<modelVersion>4.0.0</modelVersion>

	<parent>
		<artifactId>nexus-osgi-pom</artifactId>
		<groupId>com.az.nexus</groupId>
		<version>7.0</version>
		<relativePath>../nexus-osgi-pom/pom.xml</relativePath>
	</parent>

	<artifactId>nexus-strikeiron-integration</artifactId>
	<packaging>bundle</packaging>

	<name>Nexus StrikeIron Integration</name>

	<properties>
		<project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
	</properties>

	<dependencies>

		<!-- Modules -->
		<dependency>
			<groupId>${project.groupId}</groupId>
			<artifactId>nexus-profile</artifactId>
			<version>${project.version}</version>
			<scope>provided</scope>
		</dependency>
		<dependency>
			<groupId>org.apache.httpcomponents</groupId>
			<artifactId>httpclient</artifactId>
			<version>4.3.1</version>
		</dependency>
		<dependency>
			<groupId>com.az.nexus</groupId>
			<artifactId>nexus-common</artifactId>
			<version>${project.version}</version>
			<type>bundle</type>
			<scope>provided</scope>
		</dependency>
	</dependencies>

	<build>
		<finalName>../../nexus-ui/src/main/content/jcr_root/apps/astrazeneca/install/${project.artifactId}</finalName>
		<plugins>
			<plugin>
				<groupId>org.apache.felix</groupId>
				<artifactId>maven-bundle-plugin</artifactId>
				<extensions>true</extensions>
				<configuration>
					<instructions>
						<Bundle-Name>Nexus StrikeIron Integration</Bundle-Name>
						<Bundle-Description>StrikeIron Email verification for the Nexus application</Bundle-Description>
						<Bundle-Activator>com.az.nexus.strikeiron.integration.Activator</Bundle-Activator>
						<Import-Package>
						!org.apache.avalon.framework.logger.*,
						!org.apache.log.*,
						*
						</Import-Package>
						<Export-Package>com.az.nexus.strikeiron.integration.*</Export-Package>
						<Embed-Dependency>*;scope=compile|runtime</Embed-Dependency>
					</instructions>
				</configuration>
			</plugin>
		</plugins>
	</build>

	<repositories>
		<repository>
			<id>adobe-cq-nexus</id>
			<name>Adobe CQ Nexus</name>
			<layout>default</layout>
			<url>http://repo.adobe.com/nexus/content/groups/public/</url>
			<releases>
				<enabled>true</enabled>
				<updatePolicy>never</updatePolicy>
			</releases>
			<snapshots>
				<enabled>true</enabled>
				<updatePolicy>never</updatePolicy>
			</snapshots>
		</repository>
	</repositories>

	<profiles>
		<profile>
			<id>deploy</id>
			<!-- Make use of this profile to upload the module Tiered Access to your 
				local CQ instance automatically. mvn clean install -P deploy -->
			<activation>
				<activeByDefault>false</activeByDefault>
			</activation>
			<properties>
				<nexus.slingUrl>http://127.0.0.1:7503/crx/repository/crx.default</nexus.slingUrl>
				<nexus.slingUrlSuffix>/apps/astrazeneca/install</nexus.slingUrlSuffix>
				<nexus.user>admin</nexus.user>
				<nexus.password>admin</nexus.password>
			</properties>
			<build>
				<plugins>
					<plugin>
						<groupId>org.apache.sling</groupId>
						<artifactId>maven-sling-plugin</artifactId>
						<version>2.1.0</version>
						<executions>
							<execution>
								<id>install-bundle</id>
								<goals>
									<goal>install</goal>
								</goals>
								<configuration>
									<slingUrl>${nexus.slingUrl}</slingUrl>
									<slingUrlSuffix>${nexus.slingUrlSuffix}</slingUrlSuffix>
									<user>${nexus.user}</user>
									<password>${nexus.password}</password>
									<usePut>true</usePut>
								</configuration>
							</execution>
						</executions>
					</plugin>
				</plugins>
			</build>
		</profile>
	</profiles>
</project>