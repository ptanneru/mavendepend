<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">

	<modelVersion>4.0.0</modelVersion>
	<artifactId>nexus-pom</artifactId>
	<packaging>pom</packaging>
	<name>Parent POM for Nexus Modules</name>

	<inceptionYear>2011</inceptionYear>
	<parent>
		<groupId>com.az.nexus</groupId>
		<artifactId>nexus-parent</artifactId>
		<version>11.0.2</version>
		<relativePath>../nexus-parent/pom.xml</relativePath>
	</parent>

	<properties>
		<project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
		<aem.version>6.5.3</aem.version>
		<osgi.version>4.2.0</osgi.version>

		<cxf.version>2.4.1</cxf.version>
		<cxf.build-utils.version>2.3.2</cxf.build-utils.version>
		<felix.version>3.2.2</felix.version>
		<spring.version>3.0.6.RELEASE</spring.version>
		<spring.osgi.version>1.2.1</spring.osgi.version>
		<zookeeper.version>3.2.1</zookeeper.version>

		<remote.service.admin.interfaces.version>1.0.0</remote.service.admin.interfaces.version>

		<servicemix.specs.version>1.8.0</servicemix.specs.version>

		<servlet.version>3.0</servlet.version>
		<log4j.version>1.2.15</log4j.version>
		<jetty.version>11.0.2.2.v20110526</jetty.version>
		<xmlschema.bundle.version>2.0</xmlschema.bundle.version>
		<xmlresolver.bundle.version>1.2_4</xmlresolver.bundle.version>
		<neethi.bundle.version>3.0.0</neethi.bundle.version>
		<wsdl4j.bundle.version>1.6.2_5</wsdl4j.bundle.version> <!-- CXF uses 1.6.2 -->
		<xmlsec.bundle.version>1.4.5_1</xmlsec.bundle.version>
		<asm.bundle.version>3.3_2</asm.bundle.version>
		<commons.pool.bundle.version>1.5.7_1</commons.pool.bundle.version>
		<woodstox.bundle.version>4.1.1</woodstox.bundle.version> <!-- CXF 2.4.1 uses 4.1.1 -->
		<jaxbimpl.bundle.version>2.1.13_2</jaxbimpl.bundle.version> <!-- CXF 2.4.1 uses 2.1.13_2 -->
		<slf4j.version>1.6.4</slf4j.version>

		<felix.plugin.version>2.3.4</felix.plugin.version>

		<ehcache.jar.version>2.4.5</ehcache.jar.version>
		<mysql-connector-java.version>5.1.6</mysql-connector-java.version>
		<hibernate.version>3.6.7.Final</hibernate.version>

		<checkstyle-plugin-version>2.9</checkstyle-plugin-version>
		<jxr-plugin-version>2.3</jxr-plugin-version>
		<cobertura-maven-plugin-version>2.7</cobertura-maven-plugin-version>

		<maven-surefire-report-plugin-version>2.18.1</maven-surefire-report-plugin-version>
		<maven-surefire-plugin-version>2.18.1</maven-surefire-plugin-version>
		<maven-assembly-plugin-version>2.3</maven-assembly-plugin-version>
		<maven-failsafe-plugin-version>2.12.3</maven-failsafe-plugin-version>
		<maven-compiler-plugin-version>2.5.1</maven-compiler-plugin-version>
		<maven-remote-resources-plugin-version>1.3</maven-remote-resources-plugin-version>
		<maven-resources-plugin-version>2.6</maven-resources-plugin-version>
		<maven-dependency-plugin-version>2.8</maven-dependency-plugin-version>
		<maven-jar-plugin-version>2.4</maven-jar-plugin-version>
	</properties>

	<build>
		<defaultGoal>install</defaultGoal>

		<pluginManagement>
			<plugins>
				<plugin>
					<groupId>org.apache.felix</groupId>
					<artifactId>maven-bundle-plugin</artifactId>
					<version>${felix.plugin.version}</version>
					<extensions>true</extensions>
				</plugin>
				<plugin>
					<groupId>org.apache.maven.plugins</groupId>
					<artifactId>maven-assembly-plugin</artifactId>
					<version>${maven-assembly-plugin-version}</version>
				</plugin>
				<plugin>
					<groupId>org.apache.maven.plugins</groupId>
					<artifactId>maven-surefire-plugin</artifactId>
					<version>${maven-surefire-plugin-version}</version>
					<configuration>
						<argLine>-XX:MaxPermSize=256m -Djava.awt.headless=true</argLine>
						<excludes>
							<exclude>com.az.nexus.esampling.EsamplingIntegrationTest.java</exclude>
							<exclude>%regex[.*[samplecentralconsumer].*Test.java]</exclude>
						</excludes>
					</configuration>
				</plugin>
				<plugin>
					<groupId>org.apache.maven.plugins</groupId>
					<artifactId>maven-failsafe-plugin</artifactId>
					<configuration>
						<argLine>-Djava.awt.headless=true</argLine>
					</configuration>
					<version>${maven-failsafe-plugin-version}</version>
				</plugin>
				<plugin>
					<groupId>org.apache.maven.plugins</groupId>
					<artifactId>maven-compiler-plugin</artifactId>
					<version>${maven-compiler-plugin-version}</version>
					<configuration>
						<source>1.8</source>
						<target>1.8</target>
						<maxmem>512M</maxmem>
						<!-- Change from false to true to avoid the need of MAVEN_OPTS, if 
							we don't need a performance test is ok -->
						<fork>true</fork>
					</configuration>
				</plugin>
				<plugin>
					<groupId>org.apache.maven.plugins</groupId>
					<artifactId>maven-antrun-plugin</artifactId>
					<version>1.6</version>
					<dependencies>
						<dependency>
							<groupId>ant</groupId>
							<artifactId>ant-trax</artifactId>
							<version>1.6.5</version>
						</dependency>
					</dependencies>
				</plugin>
				<plugin>
					<groupId>org.apache.maven.plugins</groupId>
					<artifactId>maven-resources-plugin</artifactId>
					<version>${maven-resources-plugin-version}</version>
				</plugin>
				<plugin>
					<groupId>org.apache.maven.plugins</groupId>
					<artifactId>maven-dependency-plugin</artifactId>
					<version>${maven-dependency-plugin-version}</version>
				</plugin>

				<plugin>
					<groupId>org.apache.maven.plugins</groupId>
					<artifactId>maven-remote-resources-plugin</artifactId>
					<version>${maven-remote-resources-plugin-version}</version>
					<dependencies>
						<dependency>
							<groupId>org.apache.cxf.build-utils</groupId>
							<artifactId>cxf-buildtools</artifactId>
							<version>${cxf.build-utils.version}</version>
						</dependency>
					</dependencies>
					<executions>
						<execution>
							<goals>
								<goal>process</goal>
							</goals>
							<configuration>
								<resourceBundles>
									<resourceBundle>org.apache:apache-jar-resource-bundle:1.4</resourceBundle>
								</resourceBundles>
								<supplementalModels>
									<supplementalModel>notice-supplements.xml</supplementalModel>
								</supplementalModels>
								<properties>
									<projectName>Apache CXF Distributed OSGi DSW Reference
										Implementation</projectName>
								</properties>
							</configuration>
						</execution>
					</executions>
				</plugin>
				<plugin>
					<groupId>org.apache.maven.plugins</groupId>
					<artifactId>maven-jar-plugin</artifactId>
					<version>${maven-jar-plugin-version}</version>
				</plugin>
			</plugins>
		</pluginManagement>

		<plugins>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-remote-resources-plugin</artifactId>
				<dependencies>
					<dependency>
						<groupId>org.apache.cxf.build-utils</groupId>
						<artifactId>cxf-buildtools</artifactId>
						<version>${cxf.build-utils.version}</version>
					</dependency>
				</dependencies>
				<executions>
					<execution>
						<goals>
							<goal>process</goal>
						</goals>
						<configuration>
							<resourceBundles>
								<resourceBundle>org.apache:apache-jar-resource-bundle:1.4</resourceBundle>
							</resourceBundles>
							<supplementalModels>
								<supplementalModel>notice-supplements.xml</supplementalModel>
							</supplementalModels>
							<properties>
								<projectName>Apache CXF Distributed OSGi DSW Reference
									Implementation</projectName>
							</properties>
						</configuration>
					</execution>
				</executions>
			</plugin>

			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-surefire-plugin</artifactId>
				<inherited>true</inherited>
				<configuration>
					<!-- Change from once to always to avoid the set of more -Xmx memory, 
						if we don't need a performance test is ok -->
					<forkMode>always</forkMode>
					<reportFormat>plain</reportFormat>
					<argLine>-XX:MaxPermSize=256m -Djava.awt.headless=true</argLine>
					<excludes>
						<exclude>**/*IntegrationTest.java</exclude>
					</excludes>
				</configuration>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-failsafe-plugin</artifactId>
				<configuration>
					<argLine>-Djava.awt.headless=true</argLine>
					<includes>
						<include>**/*IntegrationTest.java</include>
					</includes>
					<argLine>-Djava.awt.headless=true</argLine>
				</configuration>
				<executions>
					<execution>
						<goals>
							<goal>integration-test</goal>
							<goal>verify</goal>
						</goals>
					</execution>
				</executions>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-checkstyle-plugin</artifactId>
				<version>${checkstyle-plugin-version}</version>
				<dependencies>
					<dependency>
						<groupId>com.az.nexus</groupId>
						<artifactId>dev-support</artifactId>
						<version>${project.version}</version>
					</dependency>
				</dependencies>
				<configuration>
					<configLocation>checkstyle.xml</configLocation>
					<suppressionsLocation>suppressions.xml</suppressionsLocation>
					<includeTestSourceDirectory>true</includeTestSourceDirectory>
					<failsOnError>false</failsOnError>
					<consoleOutput>false</consoleOutput>
					<encoding>utf-8</encoding>
				</configuration>
				<executions>
					<execution>
						<id>checkstyle-code-verification</id>
						<phase>compile</phase>
						<goals>
							<goal>checkstyle</goal>
						</goals>
					</execution>
				</executions>
			</plugin>
			<plugin>
				<groupId>org.codehaus.mojo</groupId>
				<artifactId>cobertura-maven-plugin</artifactId>
				<version>${cobertura-maven-plugin-version}</version>
				<configuration>
					<formats>
						<format>html</format>
						<format>xml</format>
					</formats>
				</configuration>
				<dependencies>  
                <dependency>  
                    <groupId>org.ow2.asm</groupId>  
                    <artifactId>asm</artifactId>  
                    <version>5.0.3</version>  
                </dependency>  
            </dependencies>  
				<!-- <executions> <execution> <phase>package</phase> <goals> <goal>cobertura</goal> 
					</goals> </execution> </executions> -->
			</plugin>
		</plugins>
	</build>

	<reporting>
		<plugins>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-surefire-report-plugin</artifactId>
				<version>${maven-surefire-report-plugin-version}</version>
			</plugin>
			<plugin>
				<groupId>org.codehaus.mojo</groupId>
				<artifactId>cobertura-maven-plugin</artifactId>
				<version>${cobertura-maven-plugin-version}</version>
				<configuration>
					<formats>
						<format>html</format>
						<format>xml</format>
					</formats>
					<instrumentation>
						<ignores>
							<ignore>astrazeneca.na.*</ignore>
							<ignore>com.epsilon.*</ignore>
							<ignore>com.microsoft.*</ignore>
							<ignore>org.datacontract.schemas.*</ignore>
							<ignore>com.az.nexus.sonar.wsdl.*</ignore>
							<ignore>com.az.nexus.sonar.service.*</ignore>
							<ignore>com.az.nexus.epsilonhcp.*</ignore>
							<ignore>com.az.nexus.esampling.kiosk.promotionitemordermanager.*</ignore>
							<ignore>com.az.nexus.esampling.kiosk.promotionitempublisher.*</ignore>
							<ignore>com.az.nexus.esampling.samplecentral.*</ignore>
							<ignore>com.az.nexus.esampling.samplecentralconsumer.*</ignore>
							<ignore>com.az.nexus.healthcaremaster.binding.*</ignore>
							<ignore>com.az.nexus.healthcaremaster.consumer.*</ignore>
							<ignore>com.az.nexus.kioskconsumer.*</ignore>
						</ignores>
					</instrumentation>
					<excludes>
						<exclude>**/*Mock*.class</exclude>
						<exclude>**/*Test.class</exclude>
					</excludes>
				</configuration>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-checkstyle-plugin</artifactId>
				<version>${checkstyle-plugin-version}</version>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-jxr-plugin</artifactId>
				<version>${jxr-plugin-version}</version>
			</plugin>
		</plugins>
	</reporting>

	<repositories>
		<repository>
			<id>nexus-repository</id>
			<name>Nexus Maven Repository</name>
			<url>http://core.utils-astrazeneca.com/nexus/content/groups/public/</url>
			<releases>
				<enabled>true</enabled>
			</releases>
		</repository>
	</repositories>

	<pluginRepositories>
		<!-- <pluginRepository> <id>apache.snapshots</id> <name>Apache Maven Snapshot 
			Repository</name> <url>http://repository.apache.org/content/groups/snapshots/</url> 
			<layout>default</layout> <snapshots> <enabled>true</enabled> </snapshots> 
			<releases> <enabled>false</enabled> </releases> </pluginRepository> -->
	</pluginRepositories>
	<dependencies>
		<dependency>
			<artifactId>junit</artifactId>
			<groupId>junit</groupId>
			<version>4.9</version>
			<scope>test</scope>
		</dependency>
		<dependency>
			<groupId>org.jmock</groupId>
			<artifactId>jmock</artifactId>
			<version>2.5.1</version>
			<scope>test</scope>
		</dependency>
		<dependency>
			<groupId>org.jmock</groupId>
			<artifactId>jmock-junit4</artifactId>
			<version>2.5.1</version>
			<scope>test</scope>
		</dependency>
		<dependency>
			<groupId>org.jmock</groupId>
			<artifactId>jmock-legacy</artifactId>
			<version>2.5.1</version>
			<scope>test</scope>
		</dependency>

		<!-- Day provided dependencies -->
		<dependency>
			<artifactId>commons-codec</artifactId>
			<groupId>commons-codec</groupId>
			<version>1.4</version>
			<scope>provided</scope>
		</dependency>
		<dependency>
			<artifactId>commons-collections</artifactId>
			<groupId>commons-collections</groupId>
			<version>3.2.1</version>
			<scope>provided</scope>
		</dependency>
		<dependency>
			<artifactId>commons-io</artifactId>
			<groupId>commons-io</groupId>
			<version>1.4</version>
			<scope>provided</scope>
		</dependency>
		<dependency>
			<artifactId>commons-lang</artifactId>
			<groupId>commons-lang</groupId>
			<version>2.4</version>
			<scope>provided</scope>
		</dependency>
		<dependency>
			<groupId>org.slf4j</groupId>
			<artifactId>slf4j-api</artifactId>
			<version>${slf4j.version}</version>
			<scope>provided</scope>
		</dependency>
		<dependency>
			<groupId>org.slf4j</groupId>
			<artifactId>slf4j-log4j12</artifactId>
			<version>${slf4j.version}</version>
			<scope>provided</scope>
		</dependency>
		<dependency>
			<groupId>org.slf4j</groupId>
			<artifactId>jcl-over-slf4j</artifactId>
			<version>${slf4j.version}</version>
			<scope>provided</scope>
		</dependency>
		<dependency>
			<groupId>log4j</groupId>
			<artifactId>log4j</artifactId>
			<version>1.2.16</version>
			<scope>provided</scope>
			<!-- <exclusions> <exclusion> <groupId>javax.mail</groupId> <artifactId>mail</artifactId> 
				</exclusion> <exclusion> <groupId>javax.jms</groupId> <artifactId>jms</artifactId> 
				</exclusion> <exclusion> <groupId>com.sun.jdmk</groupId> <artifactId>jmxtools</artifactId> 
				</exclusion> <exclusion> <groupId>com.sun.jmx</groupId> <artifactId>jmxri</artifactId> 
				</exclusion> </exclusions> -->
		</dependency>
		<dependency>
			<groupId>com.adobe.aem</groupId>
			<artifactId>uber-jar</artifactId>
			<version>6.5.3</version>
			<scope>provided</scope>
			<classifier>apis</classifier>
		</dependency>
	</dependencies>

	<profiles>
		<profile>
			<id>ci</id>
			<build>
				<plugins>
					<plugin>
						<groupId>org.codehaus.mojo</groupId>
						<artifactId>cobertura-maven-plugin</artifactId>
						<version>${cobertura-maven-plugin-version}</version>
						<configuration>
							<formats>
								<format>html</format>
								<format>xml</format>
							</formats>
						</configuration>
						<executions>
							<execution>
								<phase>package</phase>
								<goals>
									<goal>cobertura</goal>
								</goals>
							</execution>
						</executions>
					</plugin>
				</plugins>
			</build>
		</profile>
	</profiles>
	<!-- <build> <pluginManagement> <plugin> <groupId>org.apache.felix</groupId> 
		<artifactId>maven-bundle-plugin</artifactId> <version>${felix.plugin.version}</version> 
		<extensions>true</extensions> </plugin> </pluginManagement> </build> <build> 
		<plugins> <plugin> <artifactId>maven-release-plugin</artifactId> <version>2.0</version> 
		<configuration> <useReleaseProfile>false</useReleaseProfile> </configuration> 
		</plugin> </plugins> <extensions> <extension> <groupId>org.apache.maven.wagon</groupId> 
		<artifactId>wagon-webdav</artifactId> <version>1.0-beta-2</version> </extension> 
		</extensions> </build> -->
</project>
