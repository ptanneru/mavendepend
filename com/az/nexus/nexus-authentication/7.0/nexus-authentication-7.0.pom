<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">

	<modelVersion>4.0.0</modelVersion>

	<parent>
		<artifactId>nexus-osgi-pom</artifactId>
		<groupId>com.az.nexus</groupId>
		<version>7.0</version>
		<relativePath>../nexus-osgi-pom/pom.xml</relativePath>
	</parent>

	<artifactId>nexus-authentication</artifactId>
	<packaging>bundle</packaging>

	<name>Nexus Authentication</name>

	<properties>
		<project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
	</properties>

	<dependencies>

		<!-- Modules -->
		<dependency>
			<groupId>${project.groupId}</groupId>
			<artifactId>nexus-tieredaccess</artifactId>
			<version>${project.version}</version>
			<scope>provided</scope>
		</dependency>
		<dependency>
			<groupId>${project.groupId}</groupId>
			<artifactId>nexus-japan-tieredaccess</artifactId>
			<version>${project.version}</version>
			<scope>provided</scope>
		</dependency>
		<dependency>
			<groupId>${project.groupId}</groupId>
			<artifactId>nexus-profile</artifactId>
			<version>${project.version}</version>
			<scope>provided</scope>
		</dependency>
		<dependency>
			<groupId>${project.groupId}</groupId>
			<artifactId>nexus-login</artifactId>
			<version>${project.version}</version>
			<scope>provided</scope>
		</dependency>
		<dependency>
			<groupId>com.az.nexus</groupId>
			<artifactId>nexus-common</artifactId>
			<version>${project.version}</version>
			<type>bundle</type>
			<scope>provided</scope>
		</dependency>
		<dependency>
			<groupId>${project.groupId}</groupId>
			<artifactId>nexus-marketo-integration</artifactId>
			<version>${project.version}</version>
			<scope>provided</scope>
		</dependency>
		<dependency>
   			<groupId>com.az.nexus</groupId>
   			<artifactId>nexus-services</artifactId>
   			<version>${project.version}</version>
   			<type>bundle</type>
   			<scope>provided</scope>
  		</dependency>
		<dependency>
			<groupId>javax.persistence</groupId>
			<artifactId>persistence-api</artifactId>
			<version>1.0</version>
			<scope>provided</scope>
		</dependency>
		
		<!-- oak jaas -->
		<dependency>
			<groupId>org.apache.jackrabbit</groupId>
			<artifactId>oak-core</artifactId>
			<version>1.2.2</version>
			<scope>provided</scope>
			<optional>true</optional>
		</dependency>
		<dependency>
			<groupId>org.apache.felix</groupId>
			<artifactId>org.apache.felix.jaas</artifactId>
			<version>0.0.2</version>
			<scope>provided</scope>
			<optional>true</optional>
		</dependency>


		<!-- encapsulated token -->

		<dependency>
			<groupId>org.apache.oltu.oauth2</groupId>
			<artifactId>org.apache.oltu.oauth2.jwt</artifactId>
			<version>1.0.0</version>
			<scope>provided</scope>
			<optional>true</optional>
		</dependency>

		
		<dependency>
			<groupId>org.hibernate</groupId>
			<artifactId>hibernate-core</artifactId>
			<version>${hibernate.version}</version>
			<scope>test</scope>
		</dependency>
		<dependency>
			<groupId>org.hibernate</groupId>
			<artifactId>hibernate-validator</artifactId>
			<version>4.3.0.Final</version>
			<scope>test</scope>
		</dependency>
		<dependency>
			<groupId>com.az.nexus</groupId>
			<artifactId>nexus-cache</artifactId>
			<version>${project.version}</version>
			<scope>provided</scope>
		</dependency>
		<dependency>
			<groupId>com.day.cq</groupId>
			<artifactId>cq-security-api</artifactId>
			<version>5.5.0</version>
			<scope>test</scope>
		</dependency>
		<dependency>
			<groupId>com.day.crx.sling</groupId>
			<artifactId>crx-auth-token</artifactId>
			<version>2.3.15</version>
			<scope>test</scope>
		</dependency>
		<dependency>
			<groupId>com.day.cq.wcm</groupId>
			<artifactId>cq-wcm-api</artifactId>
			<version>5.7.2</version>
			<scope>test</scope>
		</dependency>
		<dependency>
			<groupId>com.day.cq</groupId>
			<artifactId>cq-search</artifactId>
			<version>5.5.4</version>
			<scope>test</scope>
		</dependency>
		<dependency>
			<groupId>com.day.cq.wcm</groupId>
			<artifactId>cq-wcm-commons</artifactId>
			<version>5.6.4</version>
			<scope>test</scope>
		</dependency>		
	</dependencies>

	<build>
		<finalName>../../nexus-ui/src/main/content/jcr_root/apps/astrazeneca/install/${project.artifactId}</finalName>
		<plugins>
			<plugin>
				<groupId>org.apache.felix</groupId>
				<artifactId>maven-bundle-plugin</artifactId>
				<extensions>true</extensions>
				<configuration>
					<instructions>
						<Bundle-Name>Nexus Authentication</Bundle-Name>
						<Bundle-Description>Authentication handlers for the Nexus application</Bundle-Description>
						<Bundle-Activator>com.az.nexus.authentication.Activator</Bundle-Activator>
						<Export-Package>com.az.nexus.authentication.*</Export-Package>
						<Embed-Dependency>*;scope=compile|runtime</Embed-Dependency>
						<Import-Package>!com.jamonapi.*,*</Import-Package>
					</instructions>
				</configuration>
			</plugin>
		</plugins>
	</build>

	<repositories>
		<repository>
			<id>adobe-cq-nexus</id>
			<name>Adobe CQ Nexus</name>
			<layout>default</layout>
			<url>http://repo.adobe.com/nexus/content/groups/public/</url>
			<releases>
				<enabled>true</enabled>
				<updatePolicy>never</updatePolicy>
			</releases>
			<snapshots>
				<enabled>true</enabled>
				<updatePolicy>never</updatePolicy>
			</snapshots>
		</repository>
	</repositories>

	<profiles>
		<profile>
			<id>deploy</id>
			<!-- Make use of this profile to upload the module Tiered Access to your 
				local CQ instance automatically. mvn clean install -P deploy -->
			<activation>
				<activeByDefault>false</activeByDefault>
			</activation>
			<properties>
				<nexus.slingUrl>http://127.0.0.1:7503/crx/repository/crx.default</nexus.slingUrl>
				<nexus.slingUrlSuffix>/apps/astrazeneca/install</nexus.slingUrlSuffix>
				<nexus.user>admin</nexus.user>
				<nexus.password>admin</nexus.password>
			</properties>
			<build>
				<plugins>
					<plugin>
						<groupId>org.apache.sling</groupId>
						<artifactId>maven-sling-plugin</artifactId>
						<version>2.1.0</version>
						<executions>
							<execution>
								<id>install-bundle</id>
								<goals>
									<goal>install</goal>
								</goals>
								<configuration>
									<slingUrl>${nexus.slingUrl}</slingUrl>
									<slingUrlSuffix>${nexus.slingUrlSuffix}</slingUrlSuffix>
									<user>${nexus.user}</user>
									<password>${nexus.password}</password>
									<usePut>true</usePut>
								</configuration>
							</execution>
						</executions>
					</plugin>
				</plugins>
			</build>
		</profile>
	</profiles>
</project>