<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">

	<modelVersion>4.0.0</modelVersion>

	<parent>
		<artifactId>nexus-osgi-pom</artifactId>
		<groupId>com.az.nexus</groupId>
		<version>11.0.8</version>
		<relativePath>../nexus-osgi-pom/pom.xml</relativePath>
	</parent>

	<artifactId>nexus-tieredaccess</artifactId>
	<packaging>bundle</packaging>

	<name>Nexus Tiered Access</name>

	<properties>
		<project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
	</properties>

	<dependencies>

		<!-- Modules -->
		<dependency>
			<groupId>${project.groupId}</groupId>
			<artifactId>nexus-smsgateway</artifactId>
			<version>${project.version}</version>
			<scope>provided</scope>
		</dependency>
		<dependency>
			<groupId>${project.groupId}</groupId>
			<artifactId>nexus-hibernate</artifactId>
			<version>${project.version}</version>
			<scope>provided</scope>
		</dependency>
		<dependency>
			<groupId>com.az.nexus</groupId>
			<artifactId>nexus-common</artifactId>
			<version>${project.version}</version>
			<type>bundle</type>
			<scope>provided</scope>
		</dependency>
		<dependency>
			<groupId>${project.groupId}</groupId>
			<artifactId>nexus-marketo-integration</artifactId>
			<version>${project.version}</version>
			<scope>provided</scope>
		</dependency>
		<dependency>
			<groupId>${project.groupId}</groupId>
			<artifactId>nexus-profile</artifactId>
			<version>${project.version}</version>
			<scope>provided</scope>
		</dependency>
		<!-- Apache Commons -->
		

		<!-- JSP -->
		
		<!-- JCR & Jackrabbit -->
		
		<dependency>
			<groupId>org.apache.jackrabbit</groupId>
			<artifactId>jackrabbit-core</artifactId>
			<version>2.10.0</version>
			<scope>provided</scope>
		</dependency>

		

		
		<dependency>
			<groupId>javax.xml</groupId>
			<artifactId>jaxb-api</artifactId>
			<version>2.1</version>
			<scope>provided</scope>
		</dependency>
		<dependency>
			<groupId>net.sourceforge.jexcelapi</groupId>
			<artifactId>jxl</artifactId>
			<version>2.6.12</version>
			<scope>provided</scope>
		</dependency>
		<dependency>
			<groupId>com.itextpdf</groupId>
			<artifactId>itextpdf</artifactId>
			<version>5.3.0</version>
			<scope>provided</scope>
		</dependency>
		
		<dependency>
			<groupId>mysql</groupId>
			<artifactId>mysql-connector-java</artifactId>
			<version>5.1.21</version>
			<scope>test</scope>
		</dependency>
	
		<dependency>
			<groupId>com.jcraft</groupId>
			<artifactId>jsch</artifactId>
			<version>0.1.50</version>
		</dependency>
		
		<dependency>
			<groupId>com.day.cq.wcm</groupId>
			<artifactId>cq-wcm-api</artifactId>
			<version>5.7.2</version>
			<scope>test</scope>
		</dependency>
		<dependency>
			<groupId>com.day.cq.wcm</groupId>
			<artifactId>cq-wcm-commons</artifactId>
			<version>5.6.4</version>
			<scope>test</scope>
		</dependency>
		<dependency>
			<groupId>com.day.cq</groupId>
			<artifactId>cq-commons</artifactId>
			<version>5.8.2</version>
			<scope>test</scope>
		</dependency>
		<dependency>
			<groupId>com.day.cq</groupId>
			<artifactId>cq-search</artifactId>
			<version>5.5.4</version>
			<scope>test</scope>
		</dependency>
		<dependency>
			<groupId>com.day.crx.sling</groupId>
			<artifactId>crx-auth-token</artifactId>
			<version>2.3.15</version>
			<scope>test</scope>
		</dependency>
		<dependency>
			 <groupId>com.day.cq.workflow</groupId>
			 <artifactId>cq-workflow-api</artifactId>
			 <version>5.4.2</version>
			 <scope>test</scope>
		</dependency>
	</dependencies>

	<build>
		<finalName>../../nexus-ui/src/main/content/jcr_root/apps/astrazeneca/install/${project.artifactId}</finalName>
		<plugins>
			<plugin>
				<groupId>org.apache.felix</groupId>
				<artifactId>maven-bundle-plugin</artifactId>
				<extensions>true</extensions>
				<configuration>
					<instructions>
						<Bundle-Name>Nexus Tiered Access</Bundle-Name>
						<Bundle-Description>Tiered access services for the Nexus application</Bundle-Description>
						<Bundle-Activator>com.az.nexus.tieredaccess.Activator</Bundle-Activator>
						<Import-Package>!com.jcraft.*,!com.itextpdf.text.*,*</Import-Package>
						<Export-Package>com.az.nexus.tieredaccess.*</Export-Package>
						<Embed-Dependency>*;scope=compile|runtime</Embed-Dependency>
					</instructions>
				</configuration>
			</plugin>
		</plugins>
		<pluginManagement>
			<plugins>
			</plugins>
		</pluginManagement>
	</build>
	<profiles>
		<profile>
			<id>deploy</id>
			<!-- Make use of this profile to upload the module Tiered Access to your 
				local CQ instance automatically. mvn clean install -P deploy -->
			<activation>
				<activeByDefault>false</activeByDefault>
			</activation>
			<properties>
				<nexus.slingUrl>http://127.0.0.1:7503/crx/repository/crx.default</nexus.slingUrl>
				<nexus.slingUrlSuffix>/apps/astrazeneca/install</nexus.slingUrlSuffix>
				<nexus.user>admin</nexus.user>
				<nexus.password>admin</nexus.password>
			</properties>
			<build>
				<plugins>
					<plugin>
						<groupId>org.apache.sling</groupId>
						<artifactId>maven-sling-plugin</artifactId>
						<version>2.1.0</version>
						<executions>
							<execution>
								<id>install-bundle</id>
								<goals>
									<goal>install</goal>
								</goals>
								<configuration>
									<slingUrl>${nexus.slingUrl}</slingUrl>
									<slingUrlSuffix>${nexus.slingUrlSuffix}</slingUrlSuffix>
									<user>${nexus.user}</user>
									<password>${nexus.password}</password>
									<usePut>true</usePut>
								</configuration>
							</execution>
						</executions>
					</plugin>
				</plugins>
			</build>
		</profile>
		<profile>
			<id>eclipse</id>
			<build>
				<pluginManagement>
					<plugins>
						<!-- This plugin holds configuration information for Maven plugins 
							that the Eclipse m2e plugin does not know how to handle. You should activate 
							the "eclipse" profile for this project and its sub-modules if you are working 
							in Eclipse as it will remove some of the error messages you see. It does 
							not affect the actual Maven build process. -->
						<plugin>
							<groupId>org.eclipse.m2e</groupId>
							<artifactId>lifecycle-mapping</artifactId>
							<version>1.0.0</version>
							<configuration>
								<lifecycleMappingMetadata>
									<pluginExecutions>
										<pluginExecution>
											<pluginExecutionFilter>
												<groupId>org.apache.maven.plugins</groupId>
												<artifactId>maven-remote-resources-plugin</artifactId>
												<goals>
													<goal>process</goal>
												</goals>
											</pluginExecutionFilter>
											<action>
												<execute>
													<runOnIncremental>false</runOnIncremental>
												</execute>
											</action>
										</pluginExecution>
									</pluginExecutions>
								</lifecycleMappingMetadata>
							</configuration>
						</plugin>
					</plugins>
				</pluginManagement>
			</build>
		</profile>
	</profiles>

</project>
