<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">
    <modelVersion>4.0.0</modelVersion>
    <!-- ====================================================================== -->
    <!-- P A R E N T  P R O J E C T  D E S C R I P T I O N                      -->
    <!-- ====================================================================== -->
    <parent>
        <groupId>com.day</groupId>
        <artifactId>day</artifactId>
        <version>6</version>
    </parent>

    <!-- ====================================================================== -->
    <!-- P R O J E C T  D E S C R I P T I O N                                   -->
    <!-- ====================================================================== -->
    <groupId>com.day.commons</groupId>
    <artifactId>parent</artifactId>
    <version>9</version>
    <packaging>pom</packaging>

    <name>Day Commons Library (Parent Project)</name>
    <description>
        The Day Commons Library provides miscellaneous libraries
        providing commons functionality used in multiple projects.
        As a collection of libraries, the Day Commons Library is
        comparable to the Apache Commons Library.
    </description>
    <inceptionYear>1993</inceptionYear>

    <!-- ====================================================================== -->
    <!-- S C M  D E F I N I T I O N                                             -->
    <!-- ====================================================================== -->
    <scm>
        <connection>scm:svn:http://svn.day.com/repos/commons/tags/parent-9</connection>
        <developerConnection>scm:svn:http://svn.day.com/repos/commons/tags/parent-9</developerConnection>
        <url>http://svn.day.com/repos/commons/tags/parent-9</url>
    </scm>

    <!-- ====================================================================== -->
    <!-- B U I L D   D E F I N I T I O N                                        -->
    <!-- ====================================================================== -->
    <build>
        <plugins>
            <plugin>
                <groupId>com.day.sling</groupId>
                <artifactId>maven-sling-plugin</artifactId>
                <executions>
                    <execution>
                        <id>bundle-deploy</id>
                        <goals>
                            <goal>deploy</goal>
                        </goals>
                    </execution>
                </executions>
                <configuration>
                    <obr>
                        http://obr.dev.day.com/
                    </obr>
                </configuration>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-release-plugin</artifactId>
                <configuration>
                    <tagBase>http://svn.day.com/repos/commons/tags</tagBase>
                </configuration>
            </plugin>
            <!-- ====================================================================== -->
            <!-- C O M P I L E R   P L U G I N                                          -->
            <!-- ====================================================================== -->
            <plugin>
              <groupId>org.apache.maven.plugins</groupId>
              <artifactId>maven-compiler-plugin</artifactId>
              <configuration>
                <compilerVersion>1.4</compilerVersion>
                <source>1.4</source>
                <target>1.4</target>
                <debug>true</debug>
                <showDeprecation>false</showDeprecation>
                <showWarnings>true</showWarnings>
                <optimize>false</optimize>
              </configuration>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-remote-resources-plugin</artifactId>
                <executions>
                    <execution>
                        <goals>
                            <goal>process</goal>
                        </goals>
                        <configuration>
                            <resourceBundles>
                                <resourceBundle>
                                    com.day.commons:day-jar-resource-bundle:1.0
                                </resourceBundle>
                            </resourceBundles>
                        </configuration>
                    </execution>
                </executions>
            </plugin>
        </plugins>

        <!-- ====================================================================== -->
        <!-- P L U G I N   M A N A G E M E N T                                      -->
        <!-- ====================================================================== -->
        <pluginManagement>
            <plugins>
                <plugin>
                    <groupId>com.day.sling</groupId>
                    <artifactId>maven-sling-plugin</artifactId>
                    <version>1.0.1</version>
                </plugin>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-surefire-plugin</artifactId>
                    <version>2.4.2</version>
                </plugin>
                <plugin>
                    <groupId>org.apache.felix</groupId>
                    <artifactId>maven-bundle-plugin</artifactId>
                    <version>1.4.0</version>
                    <inherited>true</inherited>
                    <configuration>
                        <instructions>
                            <Bundle-Category>day-commons</Bundle-Category>
                            <Bundle-SymbolicName>${pom.artifactId}</Bundle-SymbolicName>
                        </instructions>
                    </configuration>
                </plugin>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-remote-resources-plugin</artifactId>
                    <version>1.0</version>
                </plugin>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-release-plugin</artifactId>
                    <version>2.0-beta-7</version>
                    <configuration>
                        <useReleaseProfile>false</useReleaseProfile>
                        <scmCommentPrefix>#4208 - [maven-scm] :</scmCommentPrefix>
                        <goals>deploy</goals>
                    </configuration>
                </plugin>
            </plugins>
        </pluginManagement>

    </build>

    <!-- ====================================================================== -->
    <!-- D E P E N D E N C Y   M A N A G E M E N T                              -->
    <!-- ====================================================================== -->
    <dependencyManagement>
        <dependencies>
            <dependency>
                <groupId>javax.jcr</groupId>
                <artifactId>jcr</artifactId>
                <version>1.0</version>
                <scope>provided</scope>
            </dependency>

            <dependency>
                <groupId>com.day.crx</groupId>
                <artifactId>crx-api</artifactId>
                <version>1.3</version>
                <scope>provided</scope>
            </dependency>

            <dependency>
                <groupId>com.day.crx</groupId>
                <artifactId>crx-commons</artifactId>
                <version>1.3</version>
                <scope>provided</scope>
            </dependency>

            <dependency>
                <groupId>com.day.crx</groupId>
                <artifactId>crx-core</artifactId>
                <version>1.3</version>
                <scope>provided</scope>
            </dependency>

            <!-- SLF4j / Log4j -->
            <dependency>
                <groupId>org.slf4j</groupId>
                <artifactId>slf4j-api</artifactId>
                <version>1.3.0</version>
                <scope>provided</scope>
            </dependency>

            <dependency>
                <groupId>org.slf4j</groupId>
                <artifactId>slf4j-simple</artifactId>
                <version>1.3.0</version>
                <scope>provided</scope>
            </dependency>

            <dependency>
                <groupId>org.slf4j</groupId>
                <artifactId>slf4j-log4j12</artifactId>
                <version>1.3.0</version>
                <scope>provided</scope>
            </dependency>

            <dependency>
                <groupId>commons-io</groupId>
                <artifactId>commons-io</artifactId>
                <version>1.4</version>
                <scope>provided</scope>
            </dependency>

            <dependency>
                <groupId>commons-lang</groupId>
                <artifactId>commons-lang</artifactId>
                <version>2.4</version>
                <scope>provided</scope>
            </dependency>

            <dependency>
                <groupId>commons-httpclient</groupId>
                <artifactId>commons-httpclient</artifactId>
                <scope>provided</scope>
                <version>3.0.1</version>
                <exclusions>
                    <exclusion>
                        <groupId>commons-logging</groupId>
                        <artifactId>commons-logging</artifactId>
                    </exclusion>
                </exclusions>
            </dependency>

            <dependency>
                <groupId>log4j</groupId>
                <artifactId>log4j</artifactId>
                <version>1.2.8</version>
                <scope>provided</scope>
            </dependency>

            <dependency>
                <groupId>junit</groupId>
                <artifactId>junit</artifactId>
                <version>3.8.1</version>
                <scope>test</scope>
            </dependency>
        </dependencies>
    </dependencyManagement>
</project>
