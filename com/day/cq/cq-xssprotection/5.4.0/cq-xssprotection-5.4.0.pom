<?xml version="1.0"?><!--
  Copyright 1997-2009 Day Management AG
  Barfuesserplatz 6, 4001 Basel, Switzerland
  All Rights Reserved.

  This software is the confidential and proprietary information of
  Day Management AG, ("Confidential Information"). You shall not
  disclose such Confidential Information and shall use it only in
  accordance with the terms of the license agreement you entered into
  with Day.
 -->
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">
    <modelVersion>4.0.0</modelVersion>
    <!-- ======================================================================= -->
    <!-- P A R E N T   P R O J E C T                                             -->
    <!-- ======================================================================= -->
    <parent>
        <groupId>com.day.cq</groupId>
        <artifactId>parent</artifactId>
        <version>15</version>
    </parent>

    <!-- ======================================================================= -->
    <!-- P R O J E C T                                                           -->
    <!-- ======================================================================= -->
    <groupId>com.day.cq</groupId>
    <artifactId>cq-xssprotection</artifactId>
    <packaging>bundle</packaging>
    <version>5.4.0</version>

    <name>Day Communique 5 XSS Protection</name>
    <description>
        CQ5 platform bundle for providing XSS protection using the AntiSamy library.
    </description>

    <properties>
        <file.encoding>utf-8</file.encoding>
    </properties>

    <scm>
        <connection>scm:svn:http://svn.day.com/repos/cq5/tags/cq-xssprotection-5.4.0</connection>
        <developerConnection>scm:svn:http://svn.day.com/repos/cq5/tags/cq-xssprotection-5.4.0</developerConnection>
        <url>http://svn.day.com/repos/cq5/tags/cq-xssprotection-5.4.0</url>
    </scm>

    <!-- ======================================================================= -->
    <!-- B U I L D                                                               -->
    <!-- ======================================================================= -->
    <build>
        <plugins>
            <plugin>
                <groupId>org.apache.felix</groupId>
                <artifactId>maven-scr-plugin</artifactId>
            </plugin>
            <plugin>
                <groupId>org.apache.sling</groupId>
                <artifactId>maven-sling-plugin</artifactId>
                <configuration>
                    <slingUrlSuffix>/libs/cq/xssprotection/install/</slingUrlSuffix>
                </configuration>
            </plugin>
            <plugin>
                <groupId>org.apache.felix</groupId>
                <artifactId>maven-bundle-plugin</artifactId>
                <extensions>true</extensions>
                <configuration>
                    <instructions>
                        <Export-Package>
                            com.day.cq.xss,
                            com.day.cq.xss.taglib
                        </Export-Package>
                        <Import-Package>
                            !org.apache.xml.resolver.*,
                            !sun.io.*,
                            *
                        </Import-Package>
                        <Private-Package>
                            com.day.cq.xss.impl,
                            com.day.cq.xss.impl.rules,
                            com.day.cq.xss.impl.util,
                            org.owasp.validator.*,
                            javax.xml.*;-split-package:=merge-first,
                            org.apache.batik.*,
                            !org.apache.commons.logging.*,org.apache.commons.*,
                            org.apache.html.*,
                            org.apache.wml.*,
                            org.apache.xerces.*,
                            org.apache.xml.*,
                            org.apache.xmlcommons.*,
                            org.cyberneko.html.*,
                            org.w3c.css.*;-split-package:=merge-first,
                            org.w3c.dom.*;-split-package:=merge-first,
                            org.xml.sax.*;-split-package:=merge-first,
                            <!-- include AntiSamy resources; haven't found a better way to do so -->
                            !META-INF.*,!org.*,!com.*,!javax.*,!license.*,*;-split-package:=first
                        </Private-Package>
                    </instructions>
                </configuration>
            </plugin>
        </plugins>
    </build>

    <!-- ======================================================================= -->
    <!-- R E P O R T I N G                                                       -->
    <!-- ======================================================================= -->
    <reporting>
        <plugins>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-javadoc-plugin</artifactId>
                <configuration>
                    <excludePackageNames>
                        com.day.cq.xss.impl,
                        com.day.cq.xss.impl.rules,
                        com.day.cq.xss.impl.util
                    </excludePackageNames>
                </configuration>
            </plugin>
        </plugins>
    </reporting>

    <!-- ======================================================================= -->
    <!-- D E P E N D E N C I E S                                                 -->
    <!-- ======================================================================= -->
    <dependencies>
        <dependency>
            <groupId>org.owasp</groupId>
            <artifactId>antisamy</artifactId>
            <version>1.4</version>
        </dependency>

        <dependency>
            <groupId>javax.servlet</groupId>
            <artifactId>servlet-api</artifactId>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>javax.servlet</groupId>
            <artifactId>jsp-api</artifactId>
            <scope>provided</scope>
        </dependency>

        <dependency>
            <groupId>org.osgi</groupId>
            <artifactId>org.osgi.core</artifactId>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>org.osgi</groupId>
            <artifactId>org.osgi.compendium</artifactId>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>commons-collections</groupId>
            <artifactId>commons-collections</artifactId>
            <version>3.2.1</version>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>javax.jcr</groupId>
            <artifactId>jcr</artifactId>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>org.slf4j</groupId>
            <artifactId>slf4j-api</artifactId>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>org.apache.sling</groupId>
            <artifactId>org.apache.sling.jcr.api</artifactId>
            <version>2.0.4</version>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>org.apache.sling</groupId>
            <artifactId>org.apache.sling.api</artifactId>
            <version>2.0.6</version>
            <scope>provided</scope>
        </dependency>
    </dependencies>

</project>
