<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">
    <modelVersion>4.0.0</modelVersion>
    <parent>
        <groupId>com.day</groupId>
        <artifactId>day</artifactId>
        <version>5</version>
    </parent>

    <groupId>com.day.cq</groupId>
    <artifactId>cq</artifactId>
    <packaging>pom</packaging>
    <version>6</version>

    <name>Day Communique 5 (Parent Project)</name>
    <description>The parent project to the Communique 5 parts</description>
    <inceptionYear>2006</inceptionYear>

    <scm>
        <connection>scm:svn:http://svn.day.com/repos/cq5/platform/tags/cq-6</connection>
        <developerConnection>scm:svn:http://svn.day.com/repos/cq5/platform/tags/cq-6</developerConnection>
        <url>http://svn.day.com/repos/cq5/platform/tags/cq-6</url>
    </scm>

    <reporting>
        <plugins>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-javadoc-plugin</artifactId>
                <configuration>
                    <stylesheet>maven</stylesheet>
                </configuration>
            </plugin>
        </plugins>
    </reporting>

    <build>
        <plugins>
            <!-- Require Java 5 or higher for building -->
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-enforcer-plugin</artifactId>
                <executions>
                    <execution>
                        <id>enforce-java</id>
                        <goals>
                            <goal>enforce</goal>
                        </goals>
                        <configuration>
                            <rules>
                                <requireJavaVersion>
                                    <message>
                                        Cq5 must be compiled with Java 5 or higher
                                    </message>
                                    <version>1.5.0</version>
                                </requireJavaVersion>
                            </rules>
                        </configuration>
                    </execution>
                </executions>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-release-plugin</artifactId>
                <version>2.0-beta-7</version>
                <configuration>
                    <tagBase>http://svn.day.com/repos/cq5/platform/tags</tagBase>
                    <scmCommentPrefix>#4208 - [maven-scm] : </scmCommentPrefix>
                    <goals>deploy</goals>
                </configuration>
            </plugin>
        </plugins>
        
        <pluginManagement>
            <plugins>
                <!--  Environmental constraint checking (Maven Version, JDK etc.) -->
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-enforcer-plugin</artifactId>
                    <version>1.0-alpha-3</version>
                </plugin>

                <!-- Apache Felix SCR Plugin -->
                <plugin> 
                    <groupId>org.apache.felix</groupId>
                    <artifactId>maven-scr-plugin</artifactId> 
                    <version>1.0.7-r681658</version> 
                    <executions> 
                        <execution> 
                            <id>generate-scr-scrdescriptor</id> 
                            <goals> 
                                <goal>scr</goal> 
                            </goals> 
                            <configuration>
                              <!-- Private service properties for all services. -->
                                <properties>
                                    <service.vendor>Day Management AG</service.vendor>
                                </properties>
                            </configuration>
                        </execution> 
                    </executions> 
                </plugin> 
            
                <!-- Apache Sling Plugin 
                     This plugin can be used to deploy the bundles to our obr.
                -->
                <plugin> 
                    <groupId>org.apache.sling</groupId> 
                    <artifactId>maven-sling-plugin</artifactId> 
                    <version>2.0.2-incubator</version> 
<!-- 
                    <executions> 
                        <execution> 
                            <id>bundle-deploy</id> 
                            <goals> 
                                <goal>deploy</goal> 
                            </goals> 
                            <configuration> 
                                <obr>http://obr.dev.day.com/obr2</obr> 
                            </configuration> 
                        </execution> 
                    </executions> 
 -->
                </plugin>
                
                <!-- Apache Sling OCM Plugin -->
                <plugin>
                    <groupId>org.apache.sling</groupId>
                    <artifactId>maven-jcrocm-plugin</artifactId> 
                    <version>2.0.2-incubator</version> 
                    <executions> 
                        <execution> 
                            <id>generate-JCR-OCM-descriptor</id> 
                            <goals> 
                                <goal>ocm</goal> 
                            </goals> 
                        </execution> 
                    </executions> 
                </plugin>
                
                <!-- Apache Sling JSPC Plugin -->
                <plugin>
                    <groupId>org.apache.sling</groupId>
                    <artifactId>maven-jcspc-plugin</artifactId> 
                    <version>2.0.2-incubator</version> 
                    <executions>
                        <execution>
                            <id>compile-jsp</id>
                            <goals>
                                <goal>jspc</goal>
                            </goals>
                        </execution>
                    </executions>
                </plugin>

                <!-- Apache Felix Bundle Plugin -->
                <plugin>
                    <groupId>org.apache.felix</groupId>
                    <artifactId>maven-bundle-plugin</artifactId>
                    <version>1.4.1</version>
                    <inherited>true</inherited>
                </plugin>
                
                <!-- Compile for Java 5 and higher -->
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-compiler-plugin</artifactId>
                    <configuration>
                        <source>1.5</source>
                        <target>1.5</target>
                    </configuration>
                </plugin>
                
                <!-- Maven Antrun Plugin -->
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-antrun-plugin</artifactId>
                    <version>1.1</version>
                </plugin>

                <!-- Maven Buildnumber Plugin -->
                <plugin>
                  <groupId>org.codehaus.mojo</groupId>
                  <artifactId>maven-buildnumber-plugin</artifactId>
                  <version>0.9.5</version>
                </plugin>
            </plugins>
        </pluginManagement>
    </build>

    <profiles>
        <profile>
        <!-- 
          Use this profile to install the OSGi bundle 
          automatically, during development
        -->
            <id>autoInstallBundle</id>
            <activation>
                <activeByDefault>false</activeByDefault>
            </activation>
            <build>
                <plugins>
                    <plugin>
		                <groupId>org.apache.sling</groupId>
		                <artifactId>maven-sling-plugin</artifactId>
  		                <executions>
                            <execution>
                                <id>install-bundle</id>
                                <goals>
                                    <goal>install</goal>
                                </goals>
                                <configuration>
                                    <slingUrl>http://localhost:8080/cq5wcm/system/console/bundles</slingUrl>
                                </configuration>
                            </execution>
                        </executions>
                    </plugin>
                </plugins>
            </build>
        </profile>
        <profile>
        <!--
           Use this profile to generate an idea project with attached sources
           and linked modules
        -->
            <id>develop</id>
            <activation>
                <activeByDefault>false</activeByDefault>
            </activation>
            <build>
                <plugins>
                    <plugin>
                        <groupId>org.apache.maven.plugins</groupId>
                        <artifactId>maven-idea-plugin</artifactId>
                        <configuration>
                            <downloadSources>true</downloadSources>
                            <linkModules>true</linkModules>
                        </configuration>
                    </plugin>
                </plugins>
            </build>
        </profile>
  
        <profile>
        <!-- This is the release profile. -->
            <id>release</id>
            <activation>
                <activeByDefault>false</activeByDefault>
            </activation>
            <build>
                <plugins>
                    <plugin>
                        <groupId>org.apache.maven.plugins</groupId>
                        <artifactId>maven-javadoc-plugin</artifactId>
                        <executions>
                            <execution>
                                <id>attach-javadocs</id>
                                <goals>
                                    <goal>jar</goal>
                                </goals>
                            </execution>
                        </executions>
                    </plugin>
                </plugins>
            </build>
        </profile>
    </profiles>

    <dependencyManagement>
        <dependencies>
          <!-- Servlet API -->
            <dependency>
                <groupId>javax.servlet</groupId>
                <artifactId>servlet-api</artifactId>
                <version>2.4</version>
                <scope>provided</scope>
            </dependency>
          <!-- JSP API -->
            <dependency>
                <groupId>javax.servlet</groupId>
                <artifactId>jsp-api</artifactId>
                <version>2.0</version>
                <scope>provided</scope>
            </dependency>
            <!-- JSTL Dependencies -->
            <dependency>
                <groupId>javax.servlet</groupId>
                <artifactId>jstl</artifactId>
                <version>1.1.2</version>
                <scope>provided</scope>
            </dependency>


       <!-- =====================================================
            Sling Bundles
            ===================================================== -->   
          <!-- the most important bundle after the framework: logging -->
            <dependency>
                <groupId>org.apache.sling</groupId>
                <artifactId>org.apache.sling.commons.log</artifactId>
                <version>2.0.2-incubator</version>
                <scope>provided</scope>
            </dependency>

            <dependency>
                <groupId>org.apache.sling</groupId>
                <artifactId>org.apache.sling.api</artifactId>
                <version>2.0.3-incubator-r677574</version>
                <scope>provided</scope>
            </dependency>
            <dependency>
                <groupId>org.apache.sling</groupId>
                <artifactId>org.apache.sling.jcr.api</artifactId>
                <version>2.0.2-incubator</version>
                <scope>provided</scope>
            </dependency>
            <dependency>
                <groupId>org.apache.sling</groupId>
                <artifactId>org.apache.sling.jcr.base</artifactId>
                <version>2.0.2-incubator</version>
                <scope>provided</scope>
            </dependency>
	        <dependency>
	            <groupId>org.apache.sling</groupId>
	            <artifactId>org.apache.sling.engine</artifactId>
	            <version>2.0.3-incubator-r674729</version>
	            <scope>provided</scope>
	        </dependency>
	        <dependency>
	            <groupId>org.apache.sling</groupId>
	            <artifactId>org.apache.sling.servlets.resolver</artifactId>
	            <version>2.0.4-incubator</version>
	            <scope>provided</scope>
	        </dependency>
	        <dependency>
	            <groupId>org.apache.sling</groupId>
	            <artifactId>org.apache.sling.bundleresource.impl</artifactId>
	            <version>2.0.2-incubator</version>
	            <scope>provided</scope>
	        </dependency>
	        <dependency>
	            <groupId>org.apache.sling</groupId>
	            <artifactId>org.apache.sling.servlets.post</artifactId>
	            <version>2.0.3-incubator-r683245</version>
	            <scope>provided</scope>
	        </dependency>
	        <dependency>
	            <groupId>org.apache.sling</groupId>
	            <artifactId>org.apache.sling.servlets.get</artifactId>
	            <version>2.0.2-incubator</version>
	            <scope>provided</scope>
	        </dependency>
	        <dependency>
	            <groupId>org.apache.sling</groupId>
	            <artifactId>org.apache.sling.event</artifactId>
	            <version>2.0.2-incubator</version>
	            <scope>provided</scope>
	        </dependency>
	        <dependency>
	            <groupId>org.apache.sling</groupId>
	            <artifactId>org.apache.sling.commons.scheduler</artifactId>
	            <version>2.0.3-incubator-r674729</version>
	            <scope>provided</scope>
	        </dependency>
	        <dependency>
	            <groupId>org.apache.sling</groupId>
	            <artifactId>org.apache.sling.commons.threads</artifactId>
	            <version>2.0.2-incubator</version>
	            <scope>provided</scope>
	        </dependency>
	        <dependency>
	            <groupId>org.apache.sling</groupId>
	            <artifactId>org.apache.sling.i18n</artifactId>
	            <version>2.0.1-incubator</version>
	            <scope>provided</scope>
	        </dependency>
	        <dependency>
	            <groupId>org.apache.sling</groupId>
	            <artifactId>org.apache.sling.jcr.classloader</artifactId>
	            <version>2.0.2-incubator</version>
	            <scope>provided</scope>
	        </dependency>
	        <dependency>
	            <groupId>org.apache.sling</groupId>
	            <artifactId>org.apache.sling.jcr.contentloader</artifactId>
	            <version>2.0.3-incubator-r679285</version>
	            <scope>provided</scope>
	        </dependency>
            <dependency>
                <groupId>org.apache.sling</groupId>
                <artifactId>org.apache.sling.adapter</artifactId>
                <version>2.0.2-incubator</version>
                <scope>provided</scope>
            </dependency>
          <!-- Scripting -->
	        <dependency>
	            <groupId>org.apache.sling</groupId>
	            <artifactId>org.apache.sling.scripting.api</artifactId>
	            <version>2.0.2-incubator</version>
	            <scope>provided</scope>
	        </dependency>
	        <dependency>
	            <groupId>org.apache.sling</groupId>
	            <artifactId>org.apache.sling.scripting.core</artifactId>
	            <version>2.0.2-incubator</version>
	            <scope>provided</scope>
	        </dependency>
	        <dependency>
	            <groupId>org.apache.sling</groupId>
	            <artifactId>org.apache.sling.scripting.javascript</artifactId>
	            <version>2.0.2-incubator</version>
	            <scope>provided</scope>
	        </dependency>
	        <dependency>
	            <groupId>org.apache.sling</groupId>
	            <artifactId>org.apache.sling.scripting.jsp</artifactId>
	            <version>2.0.2-incubator</version>
	            <scope>provided</scope>
	        </dependency>
	        <dependency>
	            <groupId>org.apache.sling</groupId>
	            <artifactId>org.apache.sling.scripting.jsp.taglib</artifactId>
	            <version>2.0.2-incubator</version>
	            <scope>provided</scope>
	        </dependency>
	        <dependency>
	            <groupId>org.apache.sling</groupId>
	            <artifactId>org.apache.sling.commons.log</artifactId>
	            <version>2.0.2-incubator</version>
	            <scope>provided</scope>
	        </dependency>
	        <dependency>
	            <groupId>org.apache.sling</groupId>
	            <artifactId>org.apache.sling.jcr.webdav</artifactId>
	            <version>2.0.2-incubator</version>
	            <scope>provided</scope>
	        </dependency>
	        <dependency>
	            <groupId>org.apache.sling</groupId>
	            <artifactId>org.apache.sling.jcr.jackrabbit.api</artifactId>
	            <version>2.0.2-incubator</version>
	            <scope>provided</scope>
	        </dependency>
            <dependency>
                <groupId>org.apache.sling</groupId>
                <artifactId>org.apache.sling.jcr.jackrabbit.client</artifactId>
                <version>2.0.2-incubator</version>
                <scope>provided</scope>
            </dependency>
            <dependency>
                <groupId>org.apache.sling</groupId>
                <artifactId>org.apache.sling.jcr.jackrabbit.server</artifactId>
                <version>2.0.2-incubator</version>
                <scope>provided</scope>
            </dependency>
            <dependency>
                <groupId>org.apache.sling</groupId>
                <artifactId>org.apache.sling.jcr.resource</artifactId>
                <version>2.0.3-incubator-r678735</version>
                <scope>provided</scope>
            </dependency>
            <dependency>
                <groupId>org.apache.sling</groupId>
                <artifactId>org.apache.sling.jcr.ocm</artifactId>
                <version>2.0.2-incubator</version>
                <scope>provided</scope>
            </dependency>
          <!-- Sling Launchpad Base -->
            <dependency>
                <groupId>org.apache.sling</groupId>
                <artifactId>org.apache.sling.launchpad.base</artifactId>
                <version>2.0.2-incubator</version>
            </dependency>
          <!-- Sling Commons -->
            <dependency>
                <groupId>org.apache.sling</groupId>
                <artifactId>org.apache.sling.commons.json</artifactId>
                <version>2.0.2-incubator</version>
                <scope>provided</scope>
            </dependency>
            <dependency>
                <groupId>org.apache.sling</groupId>
                <artifactId>org.apache.sling.commons.testing</artifactId>
                <version>2.0.2-incubator</version>
                <scope>provided</scope>
            </dependency>
            <dependency>
                <groupId>org.apache.sling</groupId>
                <artifactId>org.apache.sling.commons.osgi</artifactId>
                <version>2.0.2-incubator</version>
                <scope>provided</scope>
            </dependency>

	   <!-- =====================================================
	        Wrapper Bundles
	        ===================================================== -->   
	
            <dependency>
                <groupId>com.day.commons.osgi.wrapper</groupId>
                <artifactId>com.day.commons.osgi.wrapper.commons-httpclient</artifactId>
                <version>3.1.0-0001</version>
                <scope>provided</scope>
            </dependency>

	        <dependency>
                <groupId>com.day.commons.osgi.wrapper</groupId>
                <artifactId>com.day.commons.osgi.wrapper.mail</artifactId>
	            <version>1.4.0-0001</version>
	            <scope>provided</scope>
	        </dependency>
	
            <dependency>
                <groupId>com.day.commons.osgi.wrapper</groupId>
                <artifactId>com.day.commons.osgi.wrapper.commons-email</artifactId>
                <version>1.1.0-0001</version>
                <scope>provided</scope>
            </dependency>

	     <!-- This is required for CQ CMS Commands -->
	        <dependency>
                <groupId>com.day.commons.osgi.wrapper</groupId>
                <artifactId>com.day.commons.osgi.wrapper.commons-beanutils</artifactId>
	            <version>1.7.0-0001</version>
	            <scope>provided</scope>
	        </dependency>
	
	    <!-- Check the following (TODO) -->
	        <dependency>
                <groupId>com.day.commons.osgi.wrapper</groupId>
                <artifactId>com.day.commons.osgi.wrapper.commons-codec</artifactId>
	            <version>1.3.0-0001</version>
	            <scope>provided</scope>
	        </dependency>
	        <dependency>
                <groupId>com.day.commons.osgi.wrapper</groupId>
                <artifactId>com.day.commons.osgi.wrapper.commons-fileupload</artifactId>
	            <version>1.1.1-0001</version>
	            <scope>provided</scope>
	        </dependency>

	   <!-- =====================================================
	        Day Commons Bundles
	        ===================================================== -->   
	
	        <dependency>
	            <groupId>com.day.commons</groupId>
	            <artifactId>day-commons-jstl</artifactId>
	            <version>1.1.2</version>
	            <scope>provided</scope>
	        </dependency>
            <dependency>
                <groupId>com.day.commons</groupId>
                <artifactId>day-commons-gfx</artifactId>
                <version>1.1.2</version>
                <scope>provided</scope>
            </dependency>
            <dependency>
                <groupId>com.day.commons</groupId>
                <artifactId>day-commons-durbo</artifactId>
                <version>2.0.0</version>
                <scope>provided</scope>
            </dependency>
            <dependency>
                <groupId>com.day.commons</groupId>
                <artifactId>day.commons.datasource.jdbcpool</artifactId>
                <version>1.0.2</version>
                <scope>provided</scope>
            </dependency>
            <dependency>
                <groupId>com.day.commons</groupId>
                <artifactId>day.commons.datasource.poolservice</artifactId>
                <version>1.0.0</version>
                <scope>provided</scope>
            </dependency>

   <!-- =====================================================
        OSGI Specification Implementations
        ===================================================== -->   
     
          <!-- OSGi -->
            <dependency>
                <groupId>org.apache.felix</groupId>
                <artifactId>org.apache.felix.framework</artifactId>
                <version>1.0.4</version>
            </dependency>
            <dependency>
                <groupId>org.apache.felix</groupId>
                <artifactId>org.osgi.core</artifactId>
                <version>1.0.1</version>
                <scope>provided</scope>
            </dependency>
            <dependency>
                <groupId>org.apache.felix</groupId>
                <artifactId>org.osgi.compendium</artifactId>
                <version>1.0.1</version>
                <scope>provided</scope>
                <exclusions>
                    <exclusion>
                        <groupId>org.apache.felix</groupId>
                        <artifactId>javax.servlet</artifactId>
                    </exclusion>
                    <exclusion>
                        <groupId>org.apache.felix</groupId>
                        <artifactId>org.osgi.foundation</artifactId>
                    </exclusion>
                </exclusions>
            </dependency>
 	      <!-- Configuration Admin -->
	        <dependency>
	            <groupId>org.apache.felix</groupId>
	            <artifactId>org.apache.felix.configadmin</artifactId>
	            <version>1.0.1</version>
	            <scope>provided</scope>
	        </dependency>

	      <!-- SCR -->
	        <dependency>
	            <groupId>org.apache.felix</groupId>
	            <artifactId>org.apache.felix.scr</artifactId>
	            <version>1.0.3-r680258</version>
	            <scope>provided</scope>
	        </dependency>

	     <!-- Event Admin -->
	        <dependency>
	            <groupId>org.apache.felix</groupId>
	            <artifactId>org.apache.felix.eventadmin</artifactId>
	            <version>1.0.0</version>
	            <scope>provided</scope>
	        </dependency>

	     <!-- Metatype -->
	        <dependency>
	            <groupId>org.apache.felix</groupId>
	            <artifactId>org.apache.felix.metatype</artifactId>
	            <version>1.0.1-r679716</version>
	            <scope>provided</scope>
	        </dependency>

	     <!-- Preferences Implementation -->
	        <dependency>
	            <groupId>org.apache.felix</groupId>
	            <artifactId>org.apache.felix.prefs</artifactId>
	            <version>1.0.2</version>
	            <scope>provided</scope>
	        </dependency>

         <!-- Web Console -->
            <dependency>
                <groupId>org.apache.felix</groupId>
                <artifactId>org.apache.felix.webconsole</artifactId>
                <version>1.0.1-r672201</version>
                <scope>provided</scope>
            </dependency>

            <dependency>
                <groupId>org.eclipse.equinox.http</groupId>
                <artifactId>servlet</artifactId>
                <version>1.0.0-v20070606</version>
            </dependency>

   <!-- =====================================================
        JCR, CRX and Jackrabbit
        ===================================================== -->   
            <dependency>
                <groupId>javax.jcr</groupId>
                <artifactId>jcr</artifactId>
                <version>1.0</version>
                <scope>provided</scope>
            </dependency>
          <dependency>
              <groupId>com.day.crx</groupId>
              <artifactId>crx-api</artifactId>
              <version>1.4.0</version>
          </dependency>
          <dependency>
              <groupId>com.day.crx</groupId>
              <artifactId>crx-commons</artifactId>
              <version>1.4.0</version>
          </dependency>
          <dependency>
              <groupId>com.day.crx</groupId>
              <artifactId>crx-core</artifactId>
              <version>1.4.0</version>
          </dependency>
            <dependency>
                <groupId>com.day.crx</groupId>
                <artifactId>crx-rmi</artifactId>
                <version>1.4.0</version>
                <scope>provided</scope>
            </dependency>
          <dependency>
              <groupId>com.day.crx</groupId>
              <artifactId>crx-statistics</artifactId>
              <version>1.4.2</version>
          </dependency>
          <dependency>
              <groupId>org.apache.jackrabbit</groupId>
              <artifactId>jackrabbit-api</artifactId>
              <version>1.4</version>
          </dependency>
          <dependency>
              <groupId>org.apache.jackrabbit</groupId>
              <artifactId>jackrabbit-jcr-commons</artifactId>
              <version>1.4.2</version>
          </dependency>
          <dependency>
              <groupId>org.apache.jackrabbit</groupId>
              <artifactId>jackrabbit-spi-commons</artifactId>
              <version>1.4</version>
              <scope>provided</scope>
          </dependency>
          <dependency>
              <groupId>org.apache.jackrabbit</groupId>
              <artifactId>jackrabbit-jcr-rmi</artifactId>
              <version>1.4.1</version>
              <scope>provided</scope>
          </dependency>
          <dependency>
              <groupId>org.apache.jackrabbit</groupId>
              <artifactId>jackrabbit-core</artifactId>
              <version>1.4.5</version>
          </dependency>
          <dependency>
              <groupId>org.apache.jackrabbit</groupId>
              <artifactId>jackrabbit-jcr-tests</artifactId>
              <version>1.4</version>
          </dependency>

            <!-- Jackrabbit OCM -->
            <dependency>
                <groupId>org.apache.jackrabbit</groupId>
                <artifactId>jackrabbit-ocm</artifactId>
                <version>1.4</version>
                <scope>provided</scope>
            </dependency>

            <!-- Apache Commons Dependencies -->
            <dependency>
                <groupId>commons-collections</groupId>
                <artifactId>commons-collections</artifactId>
                <version>3.2.1</version>
                <scope>provided</scope>
            </dependency>
            <dependency>
                <groupId>commons-io</groupId>
                <artifactId>commons-io</artifactId>
                <version>1.4</version>
                <scope>provided</scope>
            </dependency>
            <dependency>
                <groupId>commons-lang</groupId>
                <artifactId>commons-lang</artifactId>
                <version>2.4</version>
                <scope>provided</scope>
            </dependency>
	        <dependency>
	            <groupId>commons-codec</groupId>
	            <artifactId>commons-codec</artifactId>
	            <version>1.3</version>
	            <scope>provided</scope>
	        </dependency>
	        <dependency>
	            <groupId>javax.mail</groupId>
	            <artifactId>mail</artifactId>
	            <version>1.4</version>
	            <scope>provided</scope>
	        </dependency>
	        <dependency>
	            <groupId>commons-httpclient</groupId>
	            <artifactId>commons-httpclient</artifactId>
	            <version>3.1</version>
	            <scope>provided</scope>
	        </dependency>
            <dependency>
                <groupId>commons-email</groupId>
                <artifactId>commons-email</artifactId>
                <version>1.1</version>
                <scope>provided</scope>
            </dependency>

            <dependency>
                <groupId>org.slf4j</groupId>
                <artifactId>slf4j-api</artifactId>
                <version>1.5.0</version>
                <scope>provided</scope>
            </dependency>
            <dependency>
                <groupId>org.slf4j</groupId>
                <artifactId>slf4j-log4j12</artifactId>
                <version>1.5.0</version>
                <scope>provided</scope>
            </dependency>
            <dependency>
                <groupId>log4j</groupId>
                <artifactId>log4j</artifactId>
                <version>1.2.13</version>
              <scope>provided</scope>
            </dependency>

            <dependency>
                <groupId>rhino</groupId>
                <artifactId>js</artifactId>
                <version>1.6R7</version>
                <scope>provided</scope>
            </dependency>

	        <dependency>
	            <groupId>jaxen</groupId>
	            <artifactId>jaxen</artifactId>
	            <version>1.1.1</version>
                <scope>provided</scope>
	        </dependency>

            <dependency>
                <groupId>junit</groupId>
                <artifactId>junit</artifactId>
                <version>4.3</version>
                <scope>test</scope>
            </dependency>
	        <dependency>
	            <groupId>org.jmock</groupId>
	            <artifactId>jmock-junit4</artifactId>
	            <version>2.2.0</version>
	            <scope>test</scope>
	        </dependency>
	        <dependency>
	            <groupId>org.slf4j</groupId>
	            <artifactId>slf4j-simple</artifactId>
                <version>1.5.0</version>
	            <scope>test</scope>
	        </dependency>
            <dependency>
                <groupId>org.subethamail</groupId>
                <artifactId>subethasmtp-wiser</artifactId>
                <version>1.2</version>
                <scope>test</scope>
            </dependency> 

   <!-- =====================================================
        WORKFLOW
        ===================================================== -->   
            <dependency>
		        <groupId>org.drools</groupId>
		        <artifactId>drools-core</artifactId>
 		        <version>4.0.4</version>
 		        <scope>provided</scope>
            </dependency>
            <dependency>
                <groupId>org.drools</groupId>
                <artifactId>drools-compiler</artifactId>
                <version>4.0.4</version>
                <scope>provided</scope>
            </dependency>
            <dependency>
                <groupId>org.drools</groupId>
                <artifactId>drools-decisiontables</artifactId>
                <version>4.0.4</version>
                <scope>provided</scope>
            </dependency>
            <dependency>
                <groupId>org.drools</groupId>
                <artifactId>drools-jsr94</artifactId>
                <version>4.0.4</version>
                <scope>provided</scope>
            </dependency>

        </dependencies>
    </dependencyManagement>
    <!-- Add the Apache Incubator Repository.
         This is only a temporary solution; as soon as Sling is out of the
         Incubator, we will remove this (as we don't want to base our
         stuff in incubated projects. -->
    <repositories>
        <repository>
            <id>apache.incubating</id>
            <name>Apache Incubating Repository</name>
            <url>http://people.apache.org/repo/m2-incubating-repository</url>
        </repository>
    </repositories>
    
</project>
